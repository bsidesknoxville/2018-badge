namespace nstext {
  char next_chr(char c) {
    if (c >= 'Z') {
      return ' ';
    }
    if ( c < '0') {
      return '0';
    }
    return c + 1;
  }
  char prev_chr(char c) {
    if (c > 'Z' || c ==' ') {
      return 'Z';
    }
    if ( c <= '0') {
      return ' ';
    }
    return c - 1;
  }
  const int Cols = 40;
  char text[Cols+1] = "HI I AM                                 ";
  int curs =9;
  void showtext() {
      int counter = 0;
      char c;

      while(true) {
        //clear and setup screen
        theDisplay.clearDisplay();
        theDisplay.setTextSize(2);
        theDisplay.setTextColor(WHITE);

        //get button press
        if(cap_elap[1]+cap_elap[2]<cap_elap[4]+cap_elap[5]) {
             text[curs] = prev_chr(text[curs]);
             counter = 0;
        } else if (cap_elap[1]+cap_elap[2]>cap_elap[4]+cap_elap[5]) {
             text[curs] = next_chr(text[curs]);
             counter = 0;    

        } else if (cap_elap[0] == 0 && cap_elap[1] == 0 && cap_elap[2] == 0 &&
                    cap_elap[3] > 0 && cap_elap[4] == 0 && cap_elap[5] == 0) {
             counter = 0;
             curs = (curs+1)% Cols;
        } else if (cap_elap[0] > 0 && cap_elap[1] == 0 && cap_elap[2] == 0 &&
                    cap_elap[3] == 0 && cap_elap[4] == 0 && cap_elap[5] == 0) {
                      return;
                      
        }
        theDisplay.setCursor(0,0);

        //cause the cursor to blink
        c = text[curs];
        if (counter % 2) {
          text[curs] = '_';
        }
        
        myprintln(text);
        theDisplay.display();
        
        text[curs] = c;

        // stop blinking after a while
        if ( counter < 30 * 2) {
            counter += 1;
        }
        myDelayMs(80);
      }

  }
  
}

app_t App_text = {"textedit",nstext::showtext};


