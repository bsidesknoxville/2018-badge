namespace replayIR {
  
void run_replayIR() {
  unsigned int recvd_val = 0;
  // Receive Code, exit on button press
  theDisplay.clearDisplay();
  theDisplay.setTextSize(2);
  theDisplay.setTextColor(WHITE);
  theDisplay.setCursor(0,0);
  theDisplay.println("Waiting");
  theDisplay.println(" for IR");
  theDisplay.setTextSize(1);
  theDisplay.println("Press Start to Exit");
  theDisplay.display();
  theReceiver.enableIRIn();
  while (last_cap) {
    if(theReceiver.getResults()) {
      theDecoder.decode();
      theReceiver.enableIRIn();
      if(theDecoder.value) {
        recvd_val = theDecoder.value;
        theDecoder.dumpResults(true);
        break;
      }
    }
    delay(100);
  }
  theReceiver.disableIRIn();
  
  theDisplay.clearDisplay();
  theDisplay.setTextSize(2);
  theDisplay.setTextColor(WHITE);
  theDisplay.setCursor(0,0);
  theDisplay.println("Press Any");
  theDisplay.println(" move to");
  theDisplay.println(" replay");
  theDisplay.display();
  
  if(recvd_val) {
    // On any button but start game, replay code
    while (last_cap) {
      if(cap_elap[last_cap]) {
        vTaskSuspendAll();
        theSender.send(NEC,recvd_val);
        xTaskResumeAll();
      }
    }
  }
}

}

app_t App_replayIR = {"ReplayIR", replayIR::run_replayIR};
