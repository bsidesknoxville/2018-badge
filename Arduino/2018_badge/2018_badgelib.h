#ifndef __2018_BADGELIB_H__
#define __2018_BADGELIB_H__

// Badge Game Globals
extern unsigned int my_wins;
extern unsigned int my_losses;
extern unsigned int my_ties;

// Capsense Section
// ----------------
#include <Adafruit_FreeTouch.h>
extern Adafruit_FreeTouch cap[];
extern unsigned long cap_elap[];
extern byte last_cap;

// SSD1306 OLED Section
// --------------------
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
extern Adafruit_SSD1306 theDisplay;
void drawBadgeIcon(const unsigned char *, int, int);
void print2ClearedDisplay(const char *,  int, int, int);
void print2ClearedDisplay(const char *);

// TSOP382 IR Receiver Section
// ---------------------------
#include <IRLibAll.h>
extern IRrecvPCI theReceiver;
extern IRdecode theDecoder; 


// SFH-4547 IR LED Section
// -----------------------
#include <IRLibSendBase.h>    // First include the send base
//Now include only the protocols you wish to actually use.
//The lowest numbered protocol should be first but remainder 
//can be any order.
#include <IRLib_P01_NEC.h>    
#include <IRLib_P02_Sony.h>   
#include <IRLibCombo.h>     // After all protocols, include this
// All of the above automatically creates a universal sending
// class called "IRsend" containing only the protocols you want.
// Now declare an instance of that sender.
extern IRsend theSender;


// Badge icons
// -----------
#define ICON_HEIGHT 64 
#define ICON_WIDTH  64

extern const unsigned char PROGMEM rockIcon[];
extern const unsigned char PROGMEM paperIcon[];
extern const unsigned char PROGMEM scissorsIcon[];
extern const unsigned char PROGMEM spockIcon[];
extern const unsigned char PROGMEM lizzardIcon[];
extern const unsigned char *badgeIcons[];
// Badge Apps Section
// ------------------
struct app {
    char *name;
    void (*run)();
};
#define app_t struct app


// RTOS Header Section
// -------------------
#include <FreeRTOS_SAMD21.h>
#define RTOS_ERROR_LED_PIN  13 // Led Pin: Typical Arduino Board
#define RTOS_ERROR_LED_LIGHTUP_STATE  HIGH // the state that makes the led light up on your board, either low or high

#define delay(...) myDelayMs(__VA_ARGS__)

void myDelayUs(int us);
void myDelayMs(int ms);
void myDelayMsUntil(TickType_t *previousWakeTime, int ms);

void thread_watchDog( void * );
void thread_irRecv( void * );
void thread_irSend( void * );
void thread_capsense0( void * );/*
void thread_capsense1( void * );
void thread_capsense2( void * );
void thread_capsense3( void * );
void thread_capsense4( void * );
void thread_capsense5( void * );*/
void thread_screen0( void *  );

extern TaskHandle_t handle_watchDog;
extern TaskHandle_t handle_irRecv;
extern TaskHandle_t handle_irSend;
extern TaskHandle_t handle_capsense0;/*
extern TaskHandle_t handle_capsense1;
extern TaskHandle_t handle_capsense2;
extern TaskHandle_t handle_capsense3;
extern TaskHandle_t handle_capsense4;
extern TaskHandle_t handle_capsense5;*/
extern TaskHandle_t handle_screen0;

#endif

