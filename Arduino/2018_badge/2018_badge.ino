#include "2018_badgelib.h"

// Only RTOS setup in this file
void setup() 
{
  Serial.begin(115200); // Serial0 is always going. no need to begin it again.
  vNopDelayMS(1000); // prevents usb driver crash on startup, do not omit this
  //while (!Serial) ;  // Wait for serial terminal to open port before starting program or 10 seconds.
  Serial.println("Serial Started");
  // Set the led the rtos will blink when we have a fatal rtos error
  // RTOS also Needs to know if high/low is the state that turns on the led.
  // Error Blink Codes:
  //    3 blinks - Fatal Rtos Error, something bad happened. Think really hard about what you just changed.
  //    2 blinks - Malloc Failed, Happens when you couldnt create a rtos object. 
  //               Probably ran out of heap.
  //    1 blink  - Stack overflow, Task needs more bytes defined for its stack! 
  //               Use the taskMonitor thread to help gauge how much more you need
  vSetErrorLed(RTOS_ERROR_LED_PIN, RTOS_ERROR_LED_LIGHTUP_STATE);

  // Create the threads that will be managed by the rtos
  // Sets the stack size and priority of each task
  // Also initializes a handler pointer to each task, which are important to communicate with and retrieve info from tasks
  Serial.println("Making threads");
  xTaskCreate(thread_watchDog,  "Watch Dog", 256, NULL, tskIDLE_PRIORITY + 8, &handle_watchDog);
  Serial.println("watchdog task created");
  /*xTaskCreate(thread_irRecv,    "IR receive", 256, NULL, tskIDLE_PRIORITY + 7, &handle_irRecv);
  Serial.println("irRecv task created");
  xTaskCreate(thread_irSend,    "IR send", 256, NULL, tskIDLE_PRIORITY + 7, &handle_irSend);
  Serial.println("irSend task created");*/
  xTaskCreate(thread_capsense0, "Capsense 0", 256, NULL, tskIDLE_PRIORITY + 7, &handle_capsense0);
  Serial.println("capsense0 task created");/*
  xTaskCreate(thread_capsense1, "Capsense 1", 256, NULL, tskIDLE_PRIORITY + 6, &handle_capsense1);
  Serial.println("capsense1 task created");
  xTaskCreate(thread_capsense2, "Capsense 2", 256, NULL, tskIDLE_PRIORITY + 6, &handle_capsense2);
  Serial.println("capsense2 task created");
  xTaskCreate(thread_capsense3, "Capsense 3", 256, NULL, tskIDLE_PRIORITY + 6, &handle_capsense3);
  Serial.println("capsense3 task created");
  xTaskCreate(thread_capsense4, "Capsense 4", 256, NULL, tskIDLE_PRIORITY + 6, &handle_capsense4);
  Serial.println("capsense4 task created");
  xTaskCreate(thread_capsense5, "Capsense 5", 256, NULL, tskIDLE_PRIORITY + 6, &handle_capsense5);
  Serial.println("capsense5 task created");*/
  xTaskCreate(thread_screen0,    "I2C[0] Screen", 256, NULL, tskIDLE_PRIORITY + 7, &handle_screen0);
  Serial.println("screen task created");
  
  Serial.println("Starting scheduler");
  // Start the RTOS, this function will never return and will schedule the tasks.
	vTaskStartScheduler();
}

//*****************************************************************
// This is now the rtos idle loop
// No rtos blocking functions allowed!
//*****************************************************************
void loop() {}
//*****************************************************************


