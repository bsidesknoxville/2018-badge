
int vals[6] = {0};
int old_vals[6] = {0};
unsigned long uptimes[6] = {0};
unsigned int avg[6] = {0};
unsigned int old_avg[6] = {0};
void thread_capsense0( void *pvParameters )
{
  int x = 0;
  unsigned long t;
  Serial.println("Thread capsense 0: Started");
  delay(1000);
  for (x=0;x<6;x++) { cap[x].begin(); cap_elap[x]=0; }
  while (1) {
    t = millis();
    for(x=0;x<6;x++) {
      vals[x] = cap[x].measure(); // get reading from cap sense pin
    }
    for(x=0;x<6;x++) {
      if(!avg[x]) {
        avg[x] = vals[x];
        old_avg[x] = avg[x];
        old_vals[x] = vals[x];
      } else {
        old_avg[x] = avg[x];
        avg[x] += (vals[x]-old_vals[x])/2;
        old_vals[x] = vals[x];
      }
      if (avg[x] > old_avg[x] + 100) {
        if(!cap_elap[x]) { cap_elap[x] = t; last_cap = x; } // start a timer
        if(uptimes[x]) { uptimes[x] = 0; } // reset bad reading
      } else if (old_avg[x] > avg[x] + 100) { // If the button was pressed but no longer is not
        cap_elap[x] = 0; // write it isn't
      }
    }
    myDelayMs(100);
  }
  // delete ourselves.
  // Have to call this or the system crashes when you reach the end bracket and then get scheduled.
  Serial.println("Thread capsense 0: Deleting");
  vTaskDelete( NULL );
}

/*
void thread_capsense1( void *pvParameters )
{
  const int x = 1;
  Serial.println("Thread capsense 1: Started");
  capsense[x].begin();
  while (1) {
      if (capsense[x].measure() > 900 && !capsense_elapsed[x]) { // get reading from cap sense pin
        capsense_elapsed[x] = millis(); // start a timer
        Serial.println("Thread capsense: Button pressed"); 
      } else if (capsense_elapsed[x]) { // If the button was pressed but no longer is not,
        capsense_elapsed[x] = 0; // write it isn't
      }
    myDelayMs(100);
  }
  // delete ourselves.
  // Have to call this or the system crashes when you reach the end bracket and then get scheduled.
  Serial.println("Thread capsense 1: Deleting");
  vTaskDelete( NULL );
}

void thread_capsense2( void *pvParameters )
{
  const int x = 2;
  Serial.println("Thread capsense 2: Started");
  capsense[x].begin();
  while (1) {
      if (capsense[x].measure() > 900 && !capsense_elapsed[x]) { // get reading from cap sense pin
        capsense_elapsed[x] = millis(); // start a timer
        Serial.println("Thread capsense: Button pressed"); 
      } else if (capsense_elapsed[x]) { // If the button was pressed but no longer is not,
        capsense_elapsed[x] = 0; // write it isn't
      }
    myDelayMs(100);
  }
  // delete ourselves.
  // Have to call this or the system crashes when you reach the end bracket and then get scheduled.
  Serial.println("Thread capsense 2: Deleting");
  vTaskDelete( NULL );
}

void thread_capsense3( void *pvParameters )
{
  const int x = 3;
  Serial.println("Thread capsense 3: Started");
  capsense[x].begin();
  while (1) {
      if (capsense[x].measure() > 900 && !capsense_elapsed[x]) { // get reading from cap sense pin
        capsense_elapsed[x] = millis(); // start a timer
        Serial.println("Thread capsense: Button pressed"); 
      } else if (capsense_elapsed[x]) { // If the button was pressed but no longer is not,
        capsense_elapsed[x] = 0; // write it isn't
      }
    myDelayMs(100);
  }
  // delete ourselves.
  // Have to call this or the system crashes when you reach the end bracket and then get scheduled.
  Serial.println("Thread capsense 3: Deleting");
  vTaskDelete( NULL );
}

void thread_capsense4( void *pvParameters )
{
  const int x = 4;
  Serial.println("Thread capsense 4: Started");
  capsense[x].begin();
  while (1) {
      if (capsense[x].measure() > 900 && !capsense_elapsed[x]) { // get reading from cap sense pin
        capsense_elapsed[x] = millis(); // start a timer
        Serial.println("Thread capsense: Button pressed"); 
      } else if (capsense_elapsed[x]) { // If the button was pressed but no longer is not,
        capsense_elapsed[x] = 0; // write it isn't
      }
    myDelayMs(100);
  }
  // delete ourselves.
  // Have to call this or the system crashes when you reach the end bracket and then get scheduled.
  Serial.println("Thread capsense 4: Deleting");
  vTaskDelete( NULL );
}

void thread_capsense5( void *pvParameters )
{
  const int x = 5;
  Serial.println("Thread capsense 5: Started");
  capsense[x].begin();
  while (1) {
      if (capsense[x].measure() > 900 && !capsense_elapsed[x]) { // get reading from cap sense pin
        capsense_elapsed[x] = millis(); // start a timer
        Serial.println("Thread capsense: Button pressed"); 
      } else if (capsense_elapsed[x]) { // If the button was pressed but no longer is not,
        capsense_elapsed[x] = 0; // write it isn't
      }
    myDelayMs(100);
  }
  // delete ourselves.
  // Have to call this or the system crashes when you reach the end bracket and then get scheduled.
  Serial.println("Thread capsense 5: Deleting");
  vTaskDelete( NULL );
}
*/

