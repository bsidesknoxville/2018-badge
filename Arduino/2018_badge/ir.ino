//#include "2018_badgelib.h"

struct ir_code { // Little Endian
  union {
    unsigned int intVal;
    struct {
      //union {  the compiler makes the ir_code structure  
        //int val : 24; become misaligned with this 
        byte bytes[3]; 
      //} payload;  supposed unholy union so do not unite.
      union {
        byte byteCtrl;
        struct {
          byte len : 5;
          byte syn : 1;
          byte fin : 1;
          byte ack : 1;
        } bitCtrl;
      } ctrl;
    } byteVal;
  } val;
};

void ir_debug(struct ir_code* c) {
    Serial.print("MSG SEND DEBUG: ");
    Serial.print(c->val.intVal,HEX);
    Serial.print(" ");
    Serial.print(c->val.byteVal.ctrl.byteCtrl,HEX);
    Serial.print(" ");
    Serial.print(c->val.byteVal.ctrl.bitCtrl.ack,HEX);
    Serial.print(c->val.byteVal.ctrl.bitCtrl.fin,HEX);
    Serial.print(c->val.byteVal.ctrl.bitCtrl.syn,HEX);
    Serial.print(" ");
    Serial.print(c->val.byteVal.ctrl.bitCtrl.len,HEX);
    Serial.print(" ");
    Serial.print(c->val.byteVal.bytes[2],HEX);
    Serial.print(c->val.byteVal.bytes[1],HEX);
    Serial.println(c->val.byteVal.bytes[0],HEX);
    //Serial.print(c->val.byteVal.payload.bytes[2],HEX);
    //Serial.print(c->val.byteVal.payload.bytes[1],HEX);
    //Serial.println(c->val.byteVal.payload.bytes[0],HEX);
    //Serial.print(" ");
    //Serial.println(c->val.byteVal.payload.val,HEX);
}

struct ir_code s_code;

static long unsigned m_z = *((int *) 0x0080A00C), m_w = *((int *) 0x0080A048);
long unsigned GetUint()
{
    m_z = 36969 * (m_z & 65535) + (m_z >> 16);
    m_w = 18000 * (m_w & 65535) + (m_w >> 16);
    return (m_z << 16) + m_w;
}

int send_ir_msg(byte *msg, unsigned int len) {
  unsigned int s_bytes = 0;
  unsigned int tries = 100;
  long unsigned randBack;
  unsigned int ack;
  if (len > 32) { len = 32; }
  s_code.val.byteVal.ctrl.bitCtrl.ack = 0;
  s_code.val.byteVal.ctrl.bitCtrl.fin = 0;
  while(s_bytes<len) {
    if (s_bytes == 0) {
      s_code.val.byteVal.ctrl.bitCtrl.syn = 1;
      s_code.val.byteVal.ctrl.bitCtrl.len = len;
    } else {
      s_code.val.byteVal.ctrl.bitCtrl.syn = 0;
      s_code.val.byteVal.ctrl.bitCtrl.len = s_bytes;  
    }
    if ( (len - s_bytes) <= 3 ) {
      s_code.val.byteVal.ctrl.bitCtrl.fin = 1;
    }
    s_code.val.byteVal.bytes[2] = msg[s_bytes++];
    if (s_bytes<len) { s_code.val.byteVal.bytes[1] = msg[s_bytes++]; }
    if (s_bytes<len) { s_code.val.byteVal.bytes[0] = msg[s_bytes++]; }
    ir_debug(&s_code);
    while (!(ack=send_ir_code_and_wait_for_ack(s_code.val.intVal)) && tries--) {
      if (!tries) { return 0; }
      randBack=(GetUint()%1500)+500; // 0.5 to 2 s
      myDelayMs(randBack); // Random Backoff
      Serial.print("delaying: ");
      Serial.println(randBack);
    }
  }
  return ack;
}

unsigned int sentRealCode = 0;

void doRealSend(unsigned int code) {
  int x;
  //taskENTER_CRITICAL (); // these don't play well.
  for (x=0;x<1;x++) {
  //Serial.print("Before I sent - ");
  //Serial.println(theReceiver.getResults());
  theReceiver.disableIRIn();
  //Serial.println("Entering Crtital Section");
  vTaskSuspendAll();
  theSender.send(NEC,code);
  xTaskResumeAll();
  //Serial.println("Exited Crtital Section");    
  theReceiver.enableIRIn();
  //Serial.print(" After I sent - ");
  //Serial.println(theReceiver.getResults());
  //vNopDelayMS(50);
  }
  //taskEXIT_CRITICAL (); // really, they don't.
}

unsigned int sentMsgCode = 0;
unsigned int sentMsgAck = 0;
unsigned long sentMsgTime = 0;
unsigned int sentMsgStatus = 0;

int send_ir_code_and_wait_for_ack(unsigned int code) {
    Serial.print("MSG SEND STAT: Sending Signal - "); // Serial Debug
    Serial.println(code,HEX);
    sentMsgCode = sentRealCode = code;
    sentMsgStatus = 0;
    sentMsgTime=millis();
    doRealSend(code);
    vTaskSuspend(NULL); // Suspend sender, receiver will resume it
    switch(sentMsgStatus) { // ACK|SEEN|TIMEOUT
      default: // unknown
      case 0: // unset
        Serial.println("MSG SEND STAT: Critical send error");
        return 0;
      case 1: // ack
        Serial.println("MSG SEND STAT: Message acknowledged");
        return sentMsgAck;
      case 4: // timeout due to not ack'd
        Serial.println("MSG SEND STAT: Message timed out");
        return 0;
    }
}

unsigned int sentAckCode = 0;
unsigned int sentAckTime = 0;
unsigned int ackTimes = 0;
void sendAck(unsigned int code, unsigned int at) {
  struct ir_code *c =  (struct ir_code*) &code;
  c->val.byteVal.ctrl.bitCtrl.ack = 1;
  sentAckCode=code;
  ackTimes=at;
  sentAckTime=millis();
  Serial.print("Ack'n: ");
  Serial.println(code,HEX);
  doRealSend(code); // Send ACK
  doRealSend(code); // Send ACK
  doRealSend(code); // Send ACK
  doRealSend(code); // Send ACK
  doRealSend(code); // Send ACK
  doRealSend(code); // Send ACK
  doRealSend(code); // Send ACK
  doRealSend(code); // Send ACK
  
}

byte r_msg[33] = {0};
unsigned int r_len = 0;
unsigned int r_bytes = 0;
void recvd_ir_msg (byte *msg) {
  Serial.print("MSG RECV STAT: Received Whole Message \"");
              Serial.print((char *) r_msg);
              Serial.println("\"");
              r_len = 0;
              r_bytes = 0;
// TODO: compare binary
              //if (!strcmp((char *) msg,"Im")) { foundPlayer=1; }
}

#define DEFAULT_ACKS_TIMES 1

struct ir_code r_code;
void thread_irRecv( void *pvParameters ) {
  unsigned long x;
  Serial.println("Thread ir recv: Started");
  theReceiver.enableIRIn();
  while (1) {
    myDelayMs(200); // should be commented out (but capsense response needs to be interrupt driven first / make capsense more stable)
    vTaskSuspendAll();
    if (theReceiver.getResults()) {
      theDecoder.decode();
      r_code = {theDecoder.value};
      theReceiver.enableIRIn();
      xTaskResumeAll();
      if (r_code.val.intVal) {
        /*if (sentRealCode && sentRealCode == r_code.val.intVal) {
          sentRealCode = 0;
          sentMsgStatus += 2; // We saw our own message.
        } else if (sentAckCode && sentAckCode == r_code.val.intVal) {
          sentAckCode = 0; // We saw our own ack.
          Serial.println("MSG RECV STAT: Saw our own ACK");
        } else */
        if (sentMsgCode && r_code.val.byteVal.ctrl.bitCtrl.ack &&
            r_code.val.byteVal.bytes[0] == s_code.val.byteVal.bytes[0] && 
            r_code.val.byteVal.bytes[1] == s_code.val.byteVal.bytes[1] &&
            r_code.val.byteVal.bytes[2] == s_code.val.byteVal.bytes[2]) {
          sentMsgCode = 0;
          sentMsgStatus += 1; // Received ACK for what was sent.
          sentMsgAck = r_code.val.intVal;
          Serial.println("MSG RECV STAT: received ack");
          vTaskResume(handle_irSend); // Resume sending thread
        } else if (!r_code.val.byteVal.ctrl.bitCtrl.ack) {
          Serial.println("MSG RECV STAT: Received a message part");
          ir_debug(&r_code);
          // May be meant for this badge. Normal Processing.
          if (r_code.val.byteVal.ctrl.bitCtrl.syn && !r_len && (r_len=r_code.val.byteVal.ctrl.bitCtrl.len)) {
            Serial.print("MSG RECV STAT: It is a start of a new message - length: ");
            Serial.println(r_len);
            // I'm expecting new msg and this is new.
            r_msg[r_bytes++] = r_code.val.byteVal.bytes[2]; // It's at least length 1
            if (r_bytes<r_len) { r_msg[r_bytes++] = r_code.val.byteVal.bytes[1]; }
            if (r_bytes<r_len) { r_msg[r_bytes++] = r_code.val.byteVal.bytes[0]; }
            if (r_bytes==r_len) { recvd_ir_msg(r_msg); } // Received whole message
            sendAck(r_code.val.intVal,DEFAULT_ACKS_TIMES);
          } else if (!r_code.val.byteVal.ctrl.bitCtrl.syn && r_len && r_bytes==r_code.val.byteVal.ctrl.bitCtrl.len) {
            Serial.print("MSG RECV STAT: It is a continuation of a new message - length: ");
            Serial.println(r_bytes);
            // Continuing an existing message
            if (r_bytes<r_len) { r_msg[r_bytes++] = r_code.val.byteVal.bytes[2]; }
            if (r_bytes<r_len) { r_msg[r_bytes++] = r_code.val.byteVal.bytes[1]; }
            if (r_bytes<r_len) { r_msg[r_bytes++] = r_code.val.byteVal.bytes[0]; }
            sendAck(r_code.val.intVal,DEFAULT_ACKS_TIMES);
            if (r_bytes==r_len) { recvd_ir_msg(r_msg); } // Received whole message
          } else if (r_code.val.byteVal.ctrl.bitCtrl.fin && r_bytes==r_len) { // Empty FIN Frame
              recvd_ir_msg(r_msg);
              sendAck(r_code.val.intVal,DEFAULT_ACKS_TIMES);
          } else if (r_len && (r_bytes>r_code.val.byteVal.ctrl.bitCtrl.len || r_len==r_code.val.byteVal.ctrl.bitCtrl.len)) {
            // Old message. Send an ack blindly. TODO: check contents against what we have stored
            sendAck(r_code.val.intVal,DEFAULT_ACKS_TIMES);
          }
        }
        Serial.print("MSG RECV STAT: Received Signal - "); // Serial Debug
        Serial.println(r_code.val.intVal,HEX);
      } else {
        Serial.println("MSG RECV STAT: Received Signal - 0");
        //ackTimes = ackTimes ? ackTimes-1 : 0; // Just in case we are messing ourselves up.
      }
    } else {
      xTaskResumeAll();
    }
    x = millis(); // Timeouts
    if ( (sentMsgCode && (x-sentMsgTime) > 1000) ) { //|| (sentRealCode && (x-sentRealCode) > 500) ) {
      sentMsgCode = sentRealCode = 0;
      sentMsgStatus += 4;
      vTaskResume(handle_irSend); // Resume sending thread
    }
    if ( sentAckCode && (x-sentAckTime) > 250 && r_len && ackTimes) {
      sendAck(sentAckCode,--ackTimes);
    }
  }
  vTaskDelete( NULL );
}

void thread_irSend( void *pvParameters ) {
  long unsigned randNum;
  long unsigned randID = GetUint();
  const int default_retry = 5;
  Serial.println("Thread ir send: Started");
  
  byte *msg = (byte *) "Hello World Idiot!";
  delay(5000);
  if (findPlayer()) {
    Serial.println("Found a player!");
    
  } else {
    Serial.println("Didn't find a player =(");
  }
  
  //if (!r_len) { send_ir_msg(msg, strlen((char*)msg)); }
  
  vTaskDelete( NULL );
}


byte foundPlayer = 0;
const long unsigned findplayer_timeout = 10000;
int findPlayer(void) {
  long unsigned t = millis();
  return send_ir_msg((byte *) "Im", 2);
}

byte exchangeChoice(byte x) {
  return send_ir_msg((byte *) "ImX", 2);
}

byte lastReceivedChoice = 0;

