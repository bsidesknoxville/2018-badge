void thread_watchDog(void *pvParameters) // Periodic Watchdog
{
    int x;
    int measurement;
    
    Serial.println("Task Monitor: Started");

    // run this task afew times before exiting forever
    while(1)
    { // Watchdog. Make sure nothing is screwy then sleep.
      myDelayMs(10000); // check in every 10 seconds
    }

    // delete ourselves.
    // Have to call this or the system crashes when you reach the end bracket and then get scheduled.
    Serial.println("Task Monitor: Deleting");
    vTaskDelete( NULL );

}

