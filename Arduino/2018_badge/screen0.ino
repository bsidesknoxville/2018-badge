//#include "2018_badgelib.h"

#define NUM_APPS 3
app_t *badge_registered[NUM_APPS];

void register_apps() {
     badge_registered[0] = &App_2018;
     badge_registered[1] = &App_text;
     badge_registered[2] = &App_replayIR;
}

void myprintln(const char *s) {
  theDisplay.println(s);
}

uint32_t sampleRate = 10;
app_t *cur_app = NULL;
unsigned int i = 0;
#define MAXLINE 20
char s[MAXLINE+1];

void thread_screen0( void *pvParameters ) 
{
  int x;
  Serial.println("Thread mainMenu: Started");
  register_apps();
  theDisplay.begin(SSD1306_SWITCHCAPVCC, 0x3C);  // initialize with the I2C addr 0x3C (for the 128x64)
  theDisplay.clearDisplay();
  theDisplay.setTextSize(1);
  theDisplay.setTextColor(WHITE);
  theDisplay.setCursor(0,0);
  theDisplay.print("Variant: ");
  theDisplay.println(VARIANT_VERSION);
  theDisplay.display();
  myDelayMs(3000); // Wait 3 seconds

  while(1) {
     theDisplay.clearDisplay();
     theDisplay.setTextSize(2);
     theDisplay.setTextColor(WHITE);
     if(cap_elap[1]+cap_elap[2]<cap_elap[4]+cap_elap[5] && i<NUM_APPS-1) {
         i++;
     } else if (cap_elap[1]+cap_elap[2]>cap_elap[4]+cap_elap[5] && i>0) {
         i--;
     } else if (cap_elap[0] == 0 && cap_elap[1] == 0 && cap_elap[2] == 0 &&
                cap_elap[3] > 0 && cap_elap[4] == 0 && cap_elap[5] == 0) {
         cur_app = badge_registered[i];
         strcpy(s,"Launching ");
         strncat(s,cur_app->name,MAXLINE-10);
         print2ClearedDisplay(s);
         cur_app->run();
     }
     if (i > 0) {
         theDisplay.setCursor(3,3);
          snprintf(s,MAXLINE,"%.*s",10,badge_registered[i-1]->name);
          myprintln(s);
     } else {
      theDisplay.setTextSize(1);
      theDisplay.setCursor(3,3);
      snprintf(s,MAXLINE,"W: %d L: %d T: %d", my_wins, my_losses, my_ties);
      myprintln(s);
      theDisplay.setTextSize(2);
     }
     theDisplay.setCursor(3,theDisplay.height()/3 + 3);
     snprintf(s,MAXLINE,"%.*s",10,badge_registered[i]->name);
     myprintln(s);
     if (i < NUM_APPS - 1) {
          theDisplay.setCursor(3,2*theDisplay.height()/3 + 3);
          snprintf(s,MAXLINE,"%.*s",10,badge_registered[i+1]->name);
          myprintln(s); 
        }
        theDisplay.drawRect(0, theDisplay.height()/3, theDisplay.width(), theDisplay.height()/3, WHITE);
      theDisplay.display();
      myDelayMs(100);
  }
  
  // delete ourselves.
  // Have to call this or the system crashes when you reach the end bracket and then get scheduled.
  Serial.println("Thread mainMenu: Deleting");
  vTaskDelete( NULL );
}

