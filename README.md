# Getting started

## Download this repository and extract it somewhere
* [Download Link](https://gitlab.com/bsidesknoxville/2018-badge/-/archive/master/2018-badge-master.zip)

## Install Arduino IDE
- http://www.arduino.cc/en/Main/Software

## Install board details
-  Open the boards manager
  - Tools -> Board: -> Board Manager
- Search for and install:
  - Arduino SAMD Boards (32-bits ARM Cortex-M0+)

## Change Sketchbook Location
- Open Preferences in the Arduino editor
- Change Sketchbook location to the Arduino folder location here.
- Restart your IDE

## Open up a sketchbook
- Open the sketchbook of your choice
  - File -> Sketchbook -> BSIDES  (for example)

## Choose the 2018 Badge Board
- Tools -> Board: -> "2018 Badge"
- Tools -> Port -> "2018 Badge"
